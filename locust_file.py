from locust import Locust, TaskSequence, task, HttpLocust, seq_task
import os
import json
import random

#HOST_NAME = os.environ['HOST']


class VikunjaTaskSet(TaskSequence):

    project_ids = []

    def on_start(self):
        # Called everytime when a simulated user starts executing TaskSet class
        self.headers = {
            'accept': "application/json",
            'content-type': "application/json",
            'Authorization': ""
        }

        # Append your login or authentication keys to header
        self.get_authentication_keys()
        self.projects = {}

    def get_authentication_keys(self):
        response = self.client.post("/login", '{"username":"testuser' + str(random.randint(1, 100)) + '", "password":"1234"}', headers=self.headers)
        data = json.loads(response.text)
        self.headers["Authorization"] = 'Bearer ' + data["token"]

    @seq_task(1)
    @task(25)
    def dashboard(self):
        self.client.get("/tasks/all", headers=self.headers)
        response = self.client.get("/projects", headers=self.headers)
        self.projects = json.loads(response.text)

    @seq_task(2)
    @task(50)
    def new_project(self):
        response = self.client.put("/projects/",
                                   data=json.dumps({"title": "project created by locust"}),
                                   headers=self.headers)
        data = json.loads(response.text)
        self.project_ids.append(data["id"])

    @seq_task(3)
    @task(500)
    def add_task(self):
        self.client.put("/projects/" + str(random.choice(self.project_ids)) + "/tasks/",
                        data=json.dumps({"text": "task created by locust"}),
                        headers=self.headers)

    @seq_task(4)
    @task(2000)
    def get_project(self):
        self.client.get("/projects/" + str(random.choice(self.project_ids)),
                        headers=self.headers)


# More Test ideas:
# * Create tasks with reminders within the next minute -> async processing
# * Create tasks with overdue due dates
# * Deeply nested projects -> db structure
# * Add a lot of webhooks and make them fire


class VikunjaLocust(HttpLocust):
    task_set = VikunjaTaskSet
    min_wait = 100
    max_wait = 1500
