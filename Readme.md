# Performance testing with locust

This repo holds a [locust](http://locust.io) configuration file to test Vikunja's performance.

### Running it

First, you'll need a running Vikunja instance. Ours will be at `http://localhost:8080`.

Before you can start testing, you need to create the nessecary users. To do so, run

```bash
~$ ./create_users.sh
```

This will create 100 users on the local instance (feel free to change the url).

Once this is complete, you can run the locustfile:

```bash
~$ locust --host=http://localhost:8080/api/v1 -f locust_file.py
```

Then head over to the url it shows you (usually something like `http://localhost:8089`) and start the test.
